# COMP519

__python version3.6__
## Before you run:
Download the initial chromosome files from the challenge website and __keep the file name as the same__  
cd minimap2  
python3 setup.py install

## Running the pipeline
everything is setup  
python3 PIPELINE.PY  
preprocess will take about five minutes to create database

## Folders
* __Outputs__ folder contains all the results for running the pipeline.
* __outputs_and_score__ folder contains all the test results.
* __saved_data__ stores all the result in binary form.

